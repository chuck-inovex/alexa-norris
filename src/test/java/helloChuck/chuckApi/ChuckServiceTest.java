package helloChuck.chuckApi;

import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class ChuckServiceTest {

    /**
     * Integration test for an happy flow
     */
    @Test
    public void testGetFact() throws IOException {
        ChuckService chuckService = new ChuckService();

        String fact = chuckService.getFact();

        assertThat(fact).isNotBlank();
    }

}
